<?php include_once "header.php"; ?>
<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h4>Форма сохранения оператора</h4>
            <div id="errordiv"></div>
            <form action="./feedback.php" method="post" id="ask-form">
                <div class="form-group">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <?php $asks = get_asks(); ?>
                      <?php foreach ($asks as $heading => $ask) : ?>
                          <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="heading<?php print $heading; ?>">
                                  <h4 class="panel-title">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php print $heading; ?>" aria-expanded="false" aria-controls="collapse<?php print $heading; ?>">
                                        <?php print $ask['name']; ?>
                                      </a>
                                  </h4>
                              </div>

                              <div id="collapse<?php print $heading; ?>" class="panel-collapse collapsing" role="tabpanel" aria-labelledby="heading<?php print $heading; ?>">
                                  <div class="panel-body">
                                    <?php foreach ($ask['values'] as $key => $value) : ?>
                                    <?php $class = ($heading == 36) ? 'novalidate' : 'checkvalidate'; ?>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="<?php print $class; ?>" name="asks<?php print $heading; ?>[]" value="<?php print $key; ?>">
                                              <?php print $value['name']; ?>
                                            </label>
                                        </div>
                                    <?php endforeach; ?>
                                  </div>
                              </div>
                          </div>
                      <?php endforeach; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="callerid">Номер клиента:</label>
                  <?php $callerid = isset($_GET['callerid']) ? $_GET['callerid'] : '' ?>
                    <input type="text" class="form-control" id="callerid"
                           name="callerid"
                           value="<?php print $callerid; ?>"
                           placeholder="Номер клиента" readonly="readonly">
                </div>
                <div class="form-group">
                    <label for="operator">Номер оператора:</label>
                  <?php $operator = isset($_GET['operator']) ? $_GET['operator'] : '' ?>
                    <input type="text" class="form-control" id="operator"
                           name="operator" value="<?php print $operator; ?>"
                           placeholder="Номер оператора" readonly="readonly">
                </div>
                <div class="form-group">
                    <label for="url-cc">Ссылка на звонок:</label>
                  <?php $url_cc = isset($_GET['url_cc']) ? $_GET['url_cc'] : '' ?>
                    <input type="text" class="form-control" id="url-cc"
                           name="url_cc" value="<?php print $url_cc; ?>"
                           placeholder="Ссылка на звонок" readonly="readonly">
                </div>
                <div class="form-group">
                    <input type="text" name="agents_name" class="form-control"
                           id="agents-name" placeholder="ФИО оператора" readonly="readonly">
                </div>
                <div class="form-group">
                  <?php $date = date('d/m/Y H:i:s', time()) ?>
                    <input type="hidden" name="date" class="form-control"
                           id="date" value="<?php print $date; ?>"
                           placeholder="Дата">
                    <p><?php print $date; ?></p>
                </div>
                <button type="submit" class="btn btn-info">Сохранить</button>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<?php include_once "footer.php"; ?>