//  Новая схема БД
create table client_data
(
  id          int auto_increment
    primary key,
  agents_name varchar(255) null,
  callerid    int          null,
  operator    varchar(255) null,
  url_cc      varchar(255) null,
  date        varchar(255) null,
  asks         text         null
);


          create table ask_data
(
  id        int auto_increment
    primary key,
  parent_id int          null,
  ask       varchar(255) null
);