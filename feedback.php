<?php include_once "header.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h4>Форма сохранения оператора</h4>
              <?php
              $callerid = isset($_POST['callerid']) ? $_POST['callerid'] : '';
              $operator = isset($_POST['operator']) ? $_POST['operator'] : '';
              $url_cc = isset($_POST['url_cc']) ? $_POST['url_cc'] : '';
              $date = isset($_POST['date']) ? $_POST['date'] : '';
              $agents_name = isset($_POST['agents_name']) ? $_POST['agents_name'] : '';

              $asks = [];
              if (isset($_POST['asks1'])) {
                $asks[] = $_POST['asks1'][0];
                //                $asks = implode(';', $_POST['asks']);
              }

              if (isset($_POST['asks10'])) {
                $asks[] = $_POST['asks10'][0];
                //                $asks = implode(';', $_POST['asks']);
              }

              $asks_data = '';
              if (!empty($asks)) {
                $asks_data = implode(';', $asks);
              }

              $values = [
                'agents_name' => $agents_name,
                'callerid'    => $callerid,
                'operator'    => $operator,
                'date'        => $date,
                'url_cc'      => $url_cc,
                'asks'        => $asks_data,
              ];
              //          var_dump($values);
              save_tasks($values);
              ?>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
<?php include_once "footer.php"; ?>