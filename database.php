<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.03.19
 * Time: 10:01
 */

// Настройки подключения к БД

function get_connection() {
  $db_user = 'tanya';
  $db_pass = 'tanya';
  $db_name = 'tanya';
  $host = 'mariadb';
  $port = 3306;
  return new PDO("mysql:host=$host;port=$port;dbname=$db_name", $db_user, $db_pass, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
}

function get_asks() {

  try {
    $db = get_connection();
    $sql = "SELECT id, parent_id, ask FROM ask_data";
    $asks = [];
    foreach ($db->query($sql) as $key => $row) {
      if (empty($row['parent_id'])) {
        $asks[$row['id']]['name'] = $row['ask'];
      }
      else {
        $asks[$row['parent_id']]['values'][$row['id']]['name'] = $row['ask'];
      }
    }
    $conn = NULL;
    return $asks;

  } catch (PDOException $e) {
    $message = "Error!:" . $e->getMessage();
    echo '<div class="alert alert-danger" role="alert">' . $message . '</div>';
    die();
  }
}

function save_tasks($values) {
  try {
    // Подключение к БД
    //            $db = new PDO("mysql:host=$host;port=$port;dbname=$db_name", $db_user, $db_pass);
    $db = get_connection();
    $sql = "INSERT INTO client_data (agents_name, callerid, operator, date, url_cc, asks) VALUES (:agents_name, :callerid, :operator, :date, :url_cc, :asks)";
    $query = $db->prepare($sql);
    $query->execute([
      ':agents_name' => $values['agents_name'],
      ':callerid' => $values['callerid'],
      ':operator' => $values['operator'],
      ':date' => $values['date'],
      ':url_cc' => $values['url_cc'],
      ':asks' => $values['asks'],
    ]);

    echo '<div class="alert alert-success" role="alert">Оператор успешно сохранён!</div>';
    $conn = NULL;
  } catch (PDOException $e) {
    $message = "Error!:" . $e->getMessage();
    echo '<div class="alert alert-danger" role="alert">' . $message . '</div>';
    die();
  }
}